class Mines {
	constructor() {

		let params = {
			rows: 8, 
			cols: 8, 
			cells: [],
			mines: 10,
			minesPointed: 0,
			mistakeMines: 0,
			playResult: true,
			minescounter: 0,
			minesleft: 0,
		};

		Object.assign(this, params);
	

		this.initialisation();
	}
	
	
	initialisation() {
		for (let r = 0; r < this.rows; r++) {
			this.cells[r] = [];
			for (let c = 0; c < this.cols; c++) {
				this.cells[r].push(new Cell({ x: c, y: r }));
			}
		}

		let madeMines = 0;
		while (madeMines < this.mines) {
			let row = Math.floor(Math.random() * this.rows);
			let col = Math.floor(Math.random() * this.cols);
			let cell = this.cells[row][col];
			if (!cell.isMine) {
				cell.value = "M";
				cell.isMine = true;
				madeMines++;
			}
		}

	
		for (let r = 0; r < this.rows; r++) {
			for (let c = 0; c < this.cols; c++) {
				if (!this.cells[r][c].isMine) {
					let mineCount = 0;
					let infoCells = this.getNearbyCells(r, c);
					for (let i = infoCells.length; i--; ) {
						if (infoCells[i].isMine) {
							mineCount++;
						}
					}
					this.cells[r][c].value = mineCount;
				}
			}
		}
		this.fieldRender();
	}


fieldRender() {
	const gameContainer = document.getElementById("minefield");

	gameContainer.innerHTML = "";
	let content = "";

		for (let r = 0; r < this.rows; r++) {
			content += '<div class="row">';
			for (let c = 0; c < this.cols; c++) {
				let currCell = this.cells[r][c];

				let add_class = "";
				let txt = "";
				// debugger
				if (currCell.isPointed) {
					add_class = "flag";
				} else if (currCell.isOpened) {
					add_class = `opened cell-${currCell.value}`;
					txt = !currCell.isMine ? currCell.value || "" : "";
				}

				content += `<div class="cell ${add_class}" data-x="${c}" data-y="${r}">${txt}</div>`;
			}
			content += "</div>";
		}

		gameContainer.innerHTML = content;
		
		document.getElementById("mine_count").textContent = this.mines;
		document.getElementById("mine_count2").textContent = this.minescounter;
		// debugger;
		document.getElementById("status_msg").style.color = "black";
		document.getElementById("status_msg").textContent = "Граємо";

	}

	getNearbyCells(r, c) {
		let results = [];
		for (let rowN = r > 0 ? -1 : 0; rowN <= (r < this.rows - 1 ? 1 : 0); rowN++) {
			for (let colN = c > 0 ? -1 : 0; colN <= (c < this.cols - 1 ? 1 : 0); colN++) { 
				console.log(r, c);
				results.push(this.cells[r + rowN][c + colN]);
			}
		}
		return results;
	}

	openMines() {
		for (let r = 0; r < this.rows; r++) {
			for (let c = 0; c < this.cols; c++) {
				if (this.cells[r][c].isMine) {
					this.cells[r][c].getTag().classList.add("opened", "cell-M");
					this.cells[r][c].getTag().textContent = "M";
					// debugger
				}
			}
		}
	}

	
	flagCell(cell) {
		if (!cell.isOpened && this.playResult) {
			let cellTag = cell.getTag(),
				mineCount = document.getElementById("mine_count"),
				mineCount2 = document.getElementById("mine_count2");
			if (!cell.isPointed) {
				cell.isPointed = true;
				cellTag.classList.add("flag");
				cellTag.textContent = "P";
				this.minescounter++;
				this.minesleft--;
				mineCount2.textContent = this.minescounter;
				mineCount.textContent = this.minesleft;
				// debugger;
				// console.dir("--", this.minescounter);
				// console.dir("left", this.minesleft);
				if (cell.isMine) {
					this.minesPointed++;
				} else {
					this.mistakeMines++;
				}
			} else {
				cell.isPointed = false;
				cellTag.classList.remove("flag");
				cellTag.textContent = "";
				this.minescounter--;
				this.minesleft++;
				mineCount2.textContent = this.minescounter;
				mineCount.textContent = this.minesleft;
				// console.dir("++", this.minescounter);
				// console.dir("left", this.minesleft);
				if (cell.isMine) {
					this.minesPointed--;
				} else {
					this.mistakeMines--;
				}
			}
			if (this.minesPointed === this.mines && this.mistakeMines === 0) {
				this.playResult = false;
				document.getElementById("status_msg").style.color = "green";
				document.getElementById("status_msg").textContent = "Ви виграли";
			}
		} else {
			if (this.minesPointed === this.mines && this.mistakeMines === 0) {
				this.playResult = false;
				document.getElementById("status_msg").style.color = "green";
				document.getElementById("status_msg").textContent = "Ви виграли";
			}
		}
	}


	openCell(cell) {
		if (!cell.isOpened && !cell.isPointed && this.playResult) {
			let cellTag = cell.getTag();
			// debugger
			cell.isOpened = true;
			cellTag.classList.add("opened", `cell-${cell.value}`);
			cellTag.textContent = !cell.isMine ? cell.value || "" : "M";

			if (cell.isMine) {
				this.playResult = false;
				// debugger
				this.openMines();
				document.getElementById("status_msg").textContent = "Ви програли";
				document.getElementById("status_msg").style.color = "#f81c1c";
			} else if (!cell.isPointed && cell.value == 0) {

				const infoCells = this.getNearbyCells(cell.y, cell.x);
				// debugger;
				for (let i = 0, len = infoCells.length; i < len; i++) {
					this.openCell(infoCells[i]);
				}
			}
		} else {
			if (this.minesPointed === this.mines && this.mistakeMines === 0) {
				document.getElementById("status_msg").style.color = "green";
				document.getElementById("status_msg").textContent = "Ви виграли";
				this.playResult = false;
			}
		}
	}
}
// {
// 	value, x, (y = 0), (isPointed = false), (isOpened = false), (isMine = false);
// }
class Cell {
	constructor({ value, x, y = 0 }) {
		let yard = {
			value,
			x,
			y,
			isPointed:false,
			isOpened:false,
			isMine:false,
		};

		Object.assign(this, yard);
	}

	getTag() {
		return document.querySelector(`.cell[data-x="${this.x}"][data-y="${this.y}"]`);
	}
}

let game;


function startGame() {
	game = new Mines();
	game.minescounter = 0;
	game.minesleft = game.mines;
	document.getElementById("status_msg").classList.remove("loose");
	// debugger
}

window.onload = function () {

	document.getElementById("new_game_btn").addEventListener("click", function () {
	startGame();
	});


	document.getElementById("minefield").addEventListener("click", function (e) {
		const target = e.target;

		if (target.classList.contains("cell")) {
			const cell = game.cells[target.getAttribute("data-y")][target.getAttribute("data-x")];

			if (!cell.isOpened && game.playResult) {
				game.openCell(cell);
				// console.log(game.minescounter)
			}
		}
	});


	document.getElementById("minefield").addEventListener("contextmenu", function (e) {
		e.preventDefault();
		const target = e.target;

		if (target.classList.contains("cell")) {
			const cell = game.cells[target.getAttribute("data-y")][target.getAttribute("data-x")];
			if (!cell.isOpened && game.playResult) {
				game.flagCell(cell);
			}
		}
	});


    startGame();
};
